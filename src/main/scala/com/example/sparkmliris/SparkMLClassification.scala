package com.example.sparkmliris

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.{LogisticRegression, RandomForestClassifier}
import org.apache.spark.ml.feature.{IndexToString, MinMaxScaler, StringIndexer, VectorAssembler}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.tuning.{ParamGridBuilder, TrainValidationSplit}
import org.apache.spark.mllib.evaluation.MulticlassMetrics

import scala.math.random
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.functions.col


/** Computes an approximation to pi */
object SparkMLClassification {
  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder
      .appName("Spark Iris ML ")
      .config("spark.master","local")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    import spark.implicits._

    println("SparkML Iris Classification")

    println("reading csv with Iris data - header as schema and cached")
    val dfIrisRaw = spark.
      read.option("inferSchema",true)
      .option("header",true).csv("src/main/resources/IRIS.csv")
      .cache()

    // работа с исходным набором данных
    val randSplitArr = dfIrisRaw.randomSplit(Array(0.7,0.3))
    val (trainData,testData) = (randSplitArr(0),randSplitArr(1))

    // подготовка набора к векторизированию фич -
    val columns:Array[String] = dfIrisRaw.columns
    val columnsLen: Int = columns.length
    val colsToDrop: Array[String] = columns.slice(columnsLen - 1,columnsLen)
    val featuresDf = dfIrisRaw.drop(colsToDrop: _*)


    featuresDf.dtypes.foreach { dt => println(f"${dt._1}%25s\t${dt._2}") }
    // вывод схемы по колонкам
    val numericColumns: Array[String] = dfIrisRaw.dtypes.filter(!_._2.equals("StringType")).map(_._1)
    featuresDf.dtypes.groupBy(_._2).mapValues(_.length).foreach(println)
    featuresDf.select(numericColumns.map(col): _*).summary().show()

    // индексатор целевой колонки - строковый
    val labelIndexer = new StringIndexer()
      .setInputCol("species")
      .setOutputCol("label")

    // деинтексатор обратный
    val labelIndexer = new StringIndexer()
      .setInputCol("prediction")
      .setOutputCol("label")

    val irisIndexed = labelIndexer.fit(dfIrisRaw).transform(dfIrisRaw)
    //сбалансированность классов
    irisIndexed.groupBy("label").count().show(5,truncate = false)

    val assembler = new VectorAssembler()
      .setInputCols(numericColumns)
      .setOutputCol("features")

    // векторизация фич по стринг индексированному df
    val asmVectorsFeatures = assembler.transform(irisIndexed)
    asmVectorsFeatures.select("features").show(5,truncate = false)


    val scaler = new MinMaxScaler()
      .setInputCol("features")
      .setOutputCol("scaledFeatures")

    val scaledFeaturesIndexedLabels = scaler.fit(asmVectorsFeatures).transform(asmVectorsFeatures)

    // назначение классификатора
    val classifier = new RandomForestClassifier()
      .setLabelCol("label")
      .setFeaturesCol("scaledFeatures")

    // модель классификатора и полученная классификация
    val rfcModel =  classifier.fit(scaledFeaturesIndexedLabels)

    // сетка параметров для настройки гиперпараметров
    val paramGrid = new ParamGridBuilder()
      .addGrid(classifier.maxDepth, Array(2, 5, 10))
      .addGrid(classifier.numTrees, Array(10, 20, 40))
      .addGrid(classifier.impurity, Array("gini", "entropy"))
      .build()

    // пайплайн для запуска модификаторов исходного множества: индексер, векторщик, нормализатор, классификатор
    val pipeline = new Pipeline()
      .setStages(Array(labelIndexer,assembler, scaler,classifier))

    // настройка гиперпараметров через TrainValidationSplit
    val trainValidationSplit = new TrainValidationSplit()
      .setEstimator(classifier)
      .setEvaluator(new MulticlassClassificationEvaluator())
      .setEstimatorParamMaps(paramGrid)
      //  80% данных на тренировку, 20% валидации
      .setTrainRatio(0.8)

    trainData.cache()
    println("train data printschema:")
    trainData.printSchema()
    val bestModel = trainValidationSplit.fit(scaledFeaturesIndexedLabels).bestModel

    // подскажите, как можно переиспользовать трубу c добавлением старой
    val prodPipeline = new Pipeline().setStages(Array(labelIndexer,assembler, scaler,bestModel))
    val pipeModel = prodPipeline.fit(trainData)
    val testResults = pipeModel.transform(testData)

    // результаты классификации предсказатель
    val predictions = testResults.select("prediction","label").map { case Row(prediction: Double, label: Double) => (prediction, label)}
    val metrics = new MulticlassMetrics(predictions.rdd)



    println(s"accuracy:${metrics.accuracy}")
    println(s"TR:${metrics.weightedTruePositiveRate}")
    println(s"Recall:${metrics.weightedRecall}")
    // сохраняем модель
    pipeModel.write.overwrite().save("src/main/resources/model/irisModelClassification")
    spark.stop()
  }
}